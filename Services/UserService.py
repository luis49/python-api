from Models.User import User
class UserService:
    def __init__(self):
        pass

    def GetUsers(self):
        users = [
            User(1, 'test', '123456'),
            User(2, 'user2', 'abcxyz'),
        ]
        return ({u.username: u for u in users}, {u.id: u for u in users})