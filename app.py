from flask import Flask, jsonify
from flask_jwt import JWT, jwt_required, current_identity
from flask_cors import CORS, cross_origin
from werkzeug.security import safe_str_cmp
from Models.User import User
from Services.UserService import UserService

userService = UserService()
(username_table, userid_table) = userService.GetUsers()

def authenticate(username, password):
    user = username_table.get(username, None)
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user

def identity(payload):
    user_id = payload['identity']
    return userid_table.get(user_id, None)

app = Flask(__name__)
app.debug = True
cors = CORS(app)
app.config['SECRET_KEY'] = 'super-secret'
app.config['CORS_HEADERS'] = 'Content-Type'

jwt = JWT(app, authenticate, identity)

@app.route('/protected')
@jwt_required()
def protected():
    return '%s' % current_identity

@app.route('/users')
@jwt_required()
def get_users():
    return jsonify([{'id':1, 'firstName':'Luis', 'lastName':'Trejo'}])

@app.route('/open')
def openData():
    return jsonify({"test":"some data","test2":"more data"})

if __name__ == '__main__':
    app.run()